<?php

class Frame_Controller_Api extends Frame_Controller {
	private $MdSession;
	private $MdUser;
	private $MdGroup;
	private $MdTask;
	private $VeJson;
	public function __construct() {
		$this->MdSession = $this->getModel('Session');
		$this->MdUser = $this->getModel('User');
		$this->MdGroup = $this->getModel('Group');
		$this->MdTask = $this->getModel('Task');
		$this->VeJson = $this->getViewEngine('Json');
	}
	public function Reg() {
		$Username = $this->Input->getPost('Username');
		$Password = $this->Input->getPost('Password');
		if((strlen($Username) < 5) || (strlen($Password) < 5)) {
			$this->VeJson->View('1', 'Username and password must be more than 5 characters.');
		} else
		if($this->MdUser->Exists($Username)) {
			$this->VeJson->View('2', 'Username is already exist.');
		} else {
			$this->MdUser->Append($Username, $Password);
			$this->VeJson->View('0', 'Reg success.');
		}
	}
	public function Login() {
		$Username = $this->Input->getPost('Username');
		$Password = $this->Input->getPost('Password');
		if($Userid = $this->MdUser->Check($Username, $Password)) {
			$this->MdSession->SetUserid($Userid);
			$this->VeJson->View('0', 'Login success.');
		} else {
			$this->VeJson->View('1', 'Username or password wrong.');
		}
	}
	public function Logout() {
		if($this->MdSession->GetUserid()) {
			$this->MdSession->DelUserid();
			$this->VeJson->View('0', 'Logout success.');
		} else {
			$this->VeJson->View('1', 'You are not login yet.');
		}
	}
	public function SetName() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Name = $this->Input->getPost('Name');
			$this->MdUser->SetName($Userid, $Name);
			$this->VeJson->View('0', 'Set name success.');
		} else {
			$this->VeJson->View('1', 'You are not login yet.');
		}
	}
	public function SetMobile() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Mobile = $this->Input->getPost('Mobile');
			preg_match("/^((13[0-9])|147|(15[0-35-9])|180|182|(18[5-9]))[0-9]{8}$/A", $Mobile, $Result);
			if(isset($Result[0]) && ($Result[0] == $Mobile)) {
				$this->MdUser->SetMobile($Userid, $Mobile);
				$this->VeJson->View('0', 'Set mobile success.');
			} else {
				$this->VeJson->View('1', 'Mobile format wrong.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function SetPassword() {
		$Password = $this->Input->getPost('Password');
		if($Userid = $this->MdSession->GetUserid()) {
			if(strlen($Password) < 5) {
				$this->VeJson->View('1', 'Password must be more than 5 characters.');
			} else {
				$this->MdUser->SetPassword($Userid, $Password);
				$this->VeJson->View('0', 'Set password success.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function UserInfo() {
		$Userid = $this->Input->getVar('Userid');
		if($UserObject = $this->MdUser->Get($Userid)) {
			$this->VeJson->View('0', $UserObject);
		} else {
			$this->VeJson->View('1', 'Useid is not exist.');
		}
	}
	public function SelfInfo() {
		if($Userid = $this->MdSession->GetUserid()) {
			$this->VeJson->View('0', $this->MdUser->Get($Userid));
		} else {
			$this->VeJson->View('1', 'You are not login yet.');
		}
	}
	public function GroupAppend() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Name = $this->Input->getPost('Name');
			if(strlen($Name) < 5) {
				$this->VeJson->View('1', 'Name must be more than 5 characters.');
			} else {
				$Groupid = $this->MdGroup->Append($Userid, $Name);
				$this->VeJson->View('0', $Groupid);
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function GroupSearch() {
		$Name = $this->Input->getPost('Name');
		$Limit = (int)($this->Input->getVar('Limit'));
		$Offset = (int)($this->Input->getVar('Offset'));
		$this->VeJson->View('0', $this->MdGroup->Search($Name, $Limit, $Offset));
	}
	public function GroupSetName() {
		$Groupid = $this->Input->getVar('Groupid');
		$Name = $this->Input->getPost('Name');
		if($Userid = $this->MdSession->GetUserid()) {
			if($this->MdGroup->Owen($Userid, $Groupid)) {
				if(strlen($Name) < 5) {
					$this->VeJson->View('1', 'Name must be more than 5 characters.');
				} else {
					$this->MdGroup->SetName($Groupid, $Name);
					$this->VeJson->View('0', 'Set name success of this group.');
				}
			} else {
				$this->VeJson->View('2', 'You are the owener of this group.');
			}
		} else {
			$this->VeJson->View('3', 'You are not login yet.');
		}
	}
	public function GroupRemove() {
		$Groupid = $this->Input->getVar('Groupid');
		if($Userid = $this->MdSession->GetUserid()) {
			if($this->MdGroup->Owen($Userid, $Groupid)) {
				$this->MdGroup->Remove($Groupid);
				$this->VeJson->View('0', 'Remove group success.');
			} else {
				$this->VeJson->View('1', 'You are not the owener of this group.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function GroupApply() {
		$Groupid = $this->Input->getVar('Groupid');
		if($Userid = $this->MdSession->GetUserid()) {
			if($this->MdGroup->Owen($Userid, $Groupid)) {
				$this->VeJson->View('1', 'You are the owener of this group.');
			} else
			if($this->MdGroup->IsMember($Groupid, $Userid)) {
				$this->VeJson->View('2', 'You have already in this group.');
			} else
			if($this->MdGroup->IsApply($Groupid, $Userid)) {
				$this->VeJson->View('3', 'Do not apply twice.');
			} else {
				$this->MdGroup->Apply($Groupid, $Userid);
				$this->VeJson->View('0', 'Apply success.');
			}
		} else {
			$this->VeJson->View('4', 'You are not login yet.');
		}
	}
	public function GroupAgree() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Relationid = $this->Input->getVar('Relationid');
			if($this->MdGroup->OwenRelation($Userid, $Relationid)) {
				$this->MdGroup->Agree($Relationid);
				$this->VeJson->View('0', 'Agree success.');
			} else {
				$this->VeJson->View('1', 'You are not the owener of this group.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function GroupList() {
		if($Userid = $this->MdSession->GetUserid()) {
			$this->VeJson->View('0', $this->MdGroup->ListGroup($Userid));	
		} else {
			$this->VeJson->View('1', 'You are not login yet.');
		}
	}
	public function GroupLeave() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Groupid = $this->Input->getVar('Groupid');
			if($this->MdGroup->IsMember($Groupid, $Userid)) {
				$this->MdGroup->Leave($Userid, $Groupid);
				$this->VeJson->View('0', 'Leave success.');
			} else {
				$this->VeJson->View('1', 'You are not the member of this group.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function GroupUser() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Groupid = $this->Input->getVar('Groupid');
			if(
				$this->MdGroup->IsMember($Groupid, $Userid) ||
				$this->MdGroup->Owen($Userid, $Groupid)		
			) {
				$this->VeJson->View('0', $this->MdGroup->ListUser($Groupid));
			} else {
				$this->VeJson->View('1', 'You are not the member of this group.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}	
	}
	public function GroupGetApply() {
		if($Userid = $this->MdSession->GetUserid()) {
			$this->VeJson->View('0', $this->MdGroup->GetApply($Userid));
		} else {
			$this->VeJson->View('1', 'You are not login yet.');
		}
	}
	public function GroupDisAgree() {
		$Relationid = $this->Input->getVar('Relationid');
		if($Userid = $this->MdSession->GetUserid()) {
			if($this->MdGroup->OwenRelation($Userid, $Relationid)) {
				$this->MdGroup->DisAgree($Relationid);
				$this->VeJson->View('0', 'DisAgree success.');
			} else {
				$this->VeJson->View('1', 'You are not the owener of this group.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function TaskAppend() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Groupid = $this->Input->getVar('Groupid');
			if($this->MdGroup->IsMember($Groupid, $Userid) || $this->MdGroup->Owen($Userid, $Groupid)) {
				$Userids = explode(',', $this->Input->getVar('Userids'));
				for($i = 0; $i < count($Userids); $i++) {
					if($this->MdGroup->IsMember($Groupid, $Userids[$i]) || $this->MdGroup->Owen($Userids[$i], $Groupid)) {
						continue;
					}
					$this->VeJson->View('1', "User with id {$Userids[$i]} is not in this group.");
					return;
				}
				if(count($Userids) > 0) {
					$Title = $this->Input->getPost('Title');
					$Content = $this->Input->getPost('Content');
					$EndTime = $this->Input->getPost('EndTime');
					if((strlen($Content) > 5) && (strlen($Title) > 5)) {
						$Taskid = $this->MdTask->Append($Groupid, $Userid, $Userids, $Title, $Content, $EndTime);
						$this->VeJson->View('0', $Taskid);
					} else {
						$this->VeJson->View('2', 'Title and content must be more than 5 characters.');
					}	
				} else {
					$this->VeJson->View('3', 'This task has no receiver.');
				}
			} else {
				$this->VeJson->View('4', "You are not in this group.");
			}
		} else {
			$this->VeJson->View('5', 'You are not login yet.');
		}
	}
	public function TaskReply() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Taskid = $this->Input->getVar('Taskid');
			if($this->MdTask->IsCreator($Userid, $Taskid) || $this->MdTask->IsReceiver($Userid, $Taskid)) {
				$this->MdTask->Reply($Userid, $Taskid, $this->Input->getPost('Content'));
				$this->VeJson->View('0', 'Reply Success.');	
			} else {
				$this->VeJson->View('1', 'You are not the member of this task.');	
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function TaskRemove() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Taskid = $this->Input->getVar('Taskid');
			if($this->MdTask->IsCreator($Userid, $Taskid)) {
				$this->MdTask->Remove($Taskid);
				$this->VeJson->View('0', 'Remove success.');	
			} else {
				$this->VeJson->View('1', 'You are not the creator of this task.');
			}	
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
	public function TaskList() {
		if($Userid = $this->MdSession->GetUserid()) {
			$this->VeJson->View('0', $this->MdTask->getList($Userid));
		} else {
			$this->VeJson->View('1', 'You are not login yet.');
		}
	}
	public function TaskListReply() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Taskid = $this->Input->getVar('Taskid');
			if($this->MdTask->IsReceiver($Userid, $Taskid) || $this->MdTask->IsCreator($Userid, $Taskid)) {
				$this->VeJson->View('0', $this->MdTask->ListReply($Taskid));
			} else {
				$this->VeJson->View('1', 'You can not view this task.');
			}
		} else {
			$this->VeJson->View('2', 'You are not login yet.');
		}
	}
}
