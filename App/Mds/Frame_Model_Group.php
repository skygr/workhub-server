<?php

class Frame_Model_Group extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
	}
	public function Append($_Userid, $_Name) {
		$Userid = $this->Db->Escape($_Userid);
		$Name = $this->Db->Escape($_Name);
		$this->Db->Query("
			INSERT INTO `group`
				SET
					`name` = '{$Name}',
					`userid` = '{$Userid}';
		");
		return $this->Db->Insertid();
	}
	public function Search($_Name, $_Limit, $_Offset) {
		$Name = '%';
		for($i = 0; $i < mb_strlen($_Name, 'utf-8'); $i++) {
			$Name .= mb_substr($_Name, $i, 1, 'utf-8') . '%';
		}
		$Name = $this->Db->Escape($Name);
		$Limit = $this->Db->Escape($_Limit);
		$Offset = $this->Db->Escape($_Offset);
		$Result = $this->Db->Query("
			SELECT
				`group`.`id`,
				`group`.`name`,
				`group`.`time`,
				`user`.`id` AS `creator_id`,
				`user`.`username` AS `creator_username`,
				`user`.`name` AS `creator_name`
			FROM
				`user`, `group`
			WHERE
				`user`.`id` = `group`.`userid` AND 
				`group`.`name` LIKE '{$Name}'
			ORDER BY `group`.`time` DESC
			LIMIT {$Limit}
			OFFSET {$Offset};
		");
		$ArrGroup = array();
		for(;$Group = $Result->fetch_object();) {
			array_push($ArrGroup, $Group);
		}
		return $ArrGroup;
	}
	public function Owen($_Userid, $_Groupid) {
		$Userid = $this->Db->Escape($_Userid);
		$Groupid = $this->Db->Escape($_Groupid);
		$Result = $this->Db->Query("
			SELECT COUNT(*) AS `count`
				FROM `group`
				WHERE
					`id` = '{$Groupid}' AND 
					`userid` = '{$Userid}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function Remove($_Groupid) {
		$Groupid = $this->Db->Escape($_Groupid);
		$this->Db->Query("
			DELETE FROM `group`
				WHERE
					`id` = '{$Groupid}';
		");
	}
	public function SetName($_Groupid, $_Name) {
		$Groupid = $this->Db->Escape($_Groupid);
		$Name = $this->Db->Escape($_Name);
		$this->Db->Query("
			UPDATE `group`
				SET
					`name` = '${Name}'
				WHERE
					`id` = '${Groupid}';
		");
	}
	public function IsMember($_Groupid, $_Userid) {
		$Groupid = $this->Db->Escape($_Groupid);
		$Userid = $this->Db->Escape($_Userid);
		$Result = $this->Db->Query("
			SELECT COUNT(*) AS `count` FROM `group_relation`
				WHERE
					`groupid` = '{$Groupid}' AND
					`userid` = '{$Userid}' AND
					(`state` > '0');
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function IsApply($_Groupid, $_Userid) {
		$Groupid = $this->Db->Escape($_Groupid);
		$Userid = $this->Db->Escape($_Userid);
		$Result = $this->Db->Query("
			SELECT COUNT(*) AS `count` FROM `group_relation`
				WHERE
					`groupid` = '{$Groupid}' AND
					`userid` = '{$Userid}' AND
					`state` = 0;
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function Apply($_Groupid, $_Userid) {
		$Groupid = $this->Db->Escape($_Groupid);
		$Userid = $this->Db->Escape($_Userid);
		$this->Db->Query("
			INSERT INTO `group_relation`
				SET
					`groupid` = '{$Groupid}',
					`userid` = '{$Userid}',
					`state` = '0';
		");
	}
	public function OwenRelation($_Userid, $_Relationid) {
		$Userid = $this->Db->Escape($_Userid);
		$Relationid = $this->Db->Escape($_Relationid);
		$Result = $this->Db->Query("
			SELECT COUNT(*) AS `count` FROM `group`, `group_relation`
				WHERE
					`group`.`userid` = '{$Userid}' AND
					`group_relation`.`groupid` = `group`.`id`;
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function Agree($_Relationid) {
		$Relationid = $this->Db->Escape($_Relationid);
		$this->Db->Query("
			UPDATE `group_relation`
				SET 
					`state` = '1'
				WHERE
					`id` = '{$Relationid}';
		");
	}
	public function Leave($_Userid, $_Groupid) {
		$Userid = $this->Db->Escape($_Userid);
		$Groupid = $this->Db->Escape($_Groupid);
		$this->Db->Query("
			DELETE FROM `group_relation`
			WHERE
				`groupid` = '{$Groupid}' AND
				`userid` = '{$Userid}';
		");
		var_dump($this->Db);
	}
	public function ListGroup($_Userid) {
		$Userid = $this->Db->Escape($_Userid);
		$Result = $this->Db->Query("
			SELECT
				`group`.`id`,
				`group`.`name`,
				`group`.`time`,
				`user`.`name` AS `creator_name`,
				`user`.`id` AS `creator_id`,
				`user`.`username` AS `creator_username`	
			FROM `group`, `user`, `group_relation`
				WHERE
					(
						 `group`.`userid` = '{$Userid}' OR
						 (
							'{$Userid}' = `group_relation`.`userid` AND
							`group`.`id` = `group_relation`.`groupid` AND
							`group_relation`.`state` > '0'
						 )
					) AND (`user`.`id` = `group`.`userid`)
		");
		$ArrGroup = array();
		for(;$Group = $Result->fetch_object();) {
			array_push($ArrGroup, $Group);
		}
		return $ArrGroup;
	}
	public function ListUser($_Groupid) {
		$Groupid = $this->Db->Escape($_Groupid);
		$Result = $this->Db->Query("
			SELECT
				`user`.`username`,
				`user`.`name`,
				`user`.`id`,
				`user`.`mobile`
			FROM
				`group_relation`, `user`, `group`
			WHERE
				`group`.id = '{$Groupid}' AND
				(
					(
						`user`.`id` = `group_relation`.`userid` AND
						`group_relation`.`state` > 0 AND
						`group_relation`.`groupid` = `group`.`id`
					) OR 
					`group`.`userid` = `user`.`id`
				)
			GROUP BY `user`.`id`;
		");
		$ArrGroup = array();
		for(;$Group = $Result->fetch_object();) {
			array_push($ArrGroup, $Group);
		}
		return $ArrGroup;
	}
	public function GetApply($_Userid) {
		$Userid = $this->Db->Escape($_Userid);
		$Result = $this->Db->Query("
			SELECT
				`user`.`username`,
				`user`.`name`,
				`user`.id AS `userid`,
				`group`.`name` AS `groupname`,
				`group`.`id` AS `groupid`,
				`group_relation`.`id`,
				`group_relation`.`time`
			FROM
				`group_relation`, `user`, `group`
			WHERE
				`group`.`userid` = '{$Userid}' AND
				`group`.`id` = `group_relation`.`groupid` AND
				`group_relation`.`userid` = `user`.`id` AND
				`group_relation`.`state` = '0';
		");
		$ArrRelation = array();
		for(;$Relation = $Result->fetch_object();) {
			array_push($ArrRelation, $Relation);
		}
		return $ArrRelation;
	}
	public function DisAgree($_Relationid) {
		$Relationid = $this->Db->Escape($_Relationid);
		$this->Db->Query("
			DELETE FROM `group_relation`
				WHERE
					`id` = '{$Relationid}' AND
					`state` = '0';
		");
	}
}
