<?php

class Frame_Model_Db extends Frame_Model {
	private $DbHost = 'localhost';
	private $DbUser = 'root';
	private $DbPswd = 'Botong88836013';
	private $DbName = 'workhub';
	private $DbHandle;
	public function __construct() {
		$this->DbHandle = new mysqli($this->DbHost, $this->DbUser, $this->DbPswd, $this->DbName);
		if($this->DbHandle->connect_errno) {
			$this->Error($this->DbHandle->connect_error);
		}
		$this->Query("SET NAMES 'utf8'; ");
	}
	public function Query($_Sql) {
		return $this->DbHandle->query($_Sql);
	}
	public function Escape($_Var) {
		return $this->DbHandle->real_escape_string($_Var);
	}
	public function InsertId() {
		return $this->DbHandle->insert_id;
	}
}
