<?php

class Frame_Model_Session extends Frame_Model {
	private $LocalSession;
	public function __construct() {
		session_start();
		$this->LocalSession = array();
	}
	public function GetUserid() {
		if(isset($_SESSION['Userid'])) {
			return $_SESSION['Userid'];
		}
		if(isset($this->LocalSession['Userid'])) {
			return $this->LocalSession['Userid'];
		}
		return (false);
	}
	public function SetUserid($Userid) {
		return ($_SESSION['Userid'] = $this->LocalSession['Userid'] = $Userid);
	}
	public function DelUserid() {
		unset($_SESSION['Userid']);
		unset($this->LocalSession['Userid']);
	}
}
