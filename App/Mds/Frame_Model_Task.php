<?php

class Frame_Model_Task extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
	}
	public function Append($_Groupid, $_Userid, $_Userids, $_Title, $_Content, $_EndTime) {
		$Groupid = $this->Db->Escape($_Groupid);
		$Userid = $this->Db->Escape($_Userid);
		$Userids = array();
		for($i = 0; $i < count($_Userids); $i++) {
			array_push($Userids, $this->Db->Escape($_Userids[$i]));
		}
		$Title = $this->Db->Escape($_Title);
		$Content = $this->Db->Escape($_Content);
		$EndTime = $this->Db->Escape($_EndTime);
		$this->Db->Query("
			INSERT INTO `task`
				SET
					`title` = '{$Title}',
					`content` = '{$Content}',
					`groupid` = '{$Groupid}',
					`userid` = '{$Userid}',
					`endtime` = '{$EndTime}',
					`state` = '0';
		");	
		$Taskid = $this->Db->Insertid();
		for($i = 0; $i < count($Userids); $i++) {
			$this->Db->Query("
				INSERT INTO `task_relation`
					SET
						`taskid` = '{$Taskid}',
						`userid` = '{$Userids[$i]}';
			");
		}
		return $Taskid;
	}
	public function IsCreator($_Userid, $_Taskid) {
		$Userid = $this->Db->Escape($_Userid);
		$Taskid = $this->Db->Escape($_Taskid);
		$Result = $this->Db->Query("
			SELECT COUNT(*) AS `count` FROM `task`
				WHERE
					`id` = '{$Taskid}' AND
					`userid` = '{$Userid}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function IsReceiver($_Userid, $_Taskid) {
		$Userid = $this->Db->Escape($_Userid);
		$Taskid = $this->Db->Escape($_Taskid);
		$Result = $this->Db->Query("
			SELECT COUNT(*) AS `count` FROM `task`
				WHERE
					`id` = '{$Taskid}' AND
					`userid` = '{$Userid}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);	
	}
	public function Reply($_Userid, $_Taskid, $_Content) {
		$Userid = $this->Db->Escape($_Userid);
		$Taskid = $this->Db->Escape($_Taskid);
		$Content = $this->Db->Escape($_Content);
		$this->Db->Query("
			INSERT INTO `task_reply`
				SET
					`taskid` = '{$Taskid}',
					`userid` = '{$Userid}',
					`content` = '{$Content}';
		");
		return $this->Db->Insertid();		
	}
	public function Remove($_Taskid) {
		$Taskid = $this->Db->Escape($_Taskid);
		$this->Db->Query("
			DELETE FROM `task` WHERE
				`id` = '{$Taskid}';
		");
	}
	public function getList($_Userid) {
		$Userid = $this->Db->Escape($_Userid);
		$Result = $this->Db->Query("
			SELECT
				`user`.`id` AS `creator_id`,
				`user`.`username` AS `creator_username`,
				`user`.`name` AS `creator_name`,
				`task`.`id`,
				`task`.`time`,
				`task`.`title`,
				`task`.`content`,
				`task`.`endtime` AS `end_time`,
				`group`.`name` AS `group_name`,
				`group`.`id` AS `group_id`	
			FROM
				`user`, `task`, `task_relation`, `group`
			WHERE
				`task`.`userid` = `user`.`id` AND
				`task`.`id` = `task_relation`.`taskid` AND
				(
					`task`.`userid` = '{$Userid}' OR
					`task_relation`.`userid` = '{$Userid}'
				) AND
				`group`.`id` = `task`.`groupid`;
		");
		$ArrTask = array();
		for(;$Task = $Result->fetch_object();) {
			array_push($ArrTask, $Task);
		}
		return $ArrTask;
	}
	public function ListReply($_Taskid) {
		$Taskid = $this->Db->Escape($_Taskid);
		$Result = $this->Db->Query("
			SELECT
				`user`.`id` AS `userid`,
				`user`.`name`,
				`user`.`username`,
				`task_reply`.`content`,
				`task_reply`.`time`,
				`task_reply`.`id`
			FROM
				`user`, `task_reply`
			WHERE
				`task_reply`.`taskid` = '{$Taskid}' AND 
				`task_reply`.`userid` = `user`.`id`;
		");
		$ArrReply = array();
		for(;$Reply = $Result->fetch_object();) {
			array_push($ArrReply, $Reply);
		}
		return $ArrReply;
	}
}
