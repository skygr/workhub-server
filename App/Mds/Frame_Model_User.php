<?php

class Frame_Model_User extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
	}
	public function Exists($_Username) {
		$Username = $this->Db->Escape($_Username);
		$Result = $this->Db->Query("
			SELECT
				COUNT(*) AS `count`
			FROM
				`user`
			WHERE
				`username` = '{$Username}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function Append($_Username, $_Password) {
		$Username = $this->Db->Escape($_Username);
		$Password = $this->Db->Escape(md5($_Password));
		if($this->Exists($Username)) {
			return false;
		} else {
			$this->Db->Query("
				INSERT
					INTO `user`
				SET
					`username` = '{$Username}',
					`password` = '{$Password}';
			");
			return true;
		}
	}
	public function Check($_Username, $_Password) {
		$Username = $this->Db->Escape($_Username);
		$Password = $this->Db->Escape(md5($_Password));
		$Result = $this->Db->Query("
			SELECT `id`
				FROM  `user`
			WHERE
				`username` = '{$Username}' AND
				`password` = '{$Password}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->id) : (false);
	}
	public function SetPassword($_Userid, $_Password) {
		$Userid = $this->Db->Escape($_Userid);
		$Password = $this->Db->Escape(md5($_Password));
		$this->Db->Query("
			UPDATE `user`
			SET	`password` = '{$Password}'
			WHERE `id` = '{$Userid}';
		");	
	}
	public function SetMobile($_Userid, $_Mobile) {
		$Userid = $this->Db->Escape($_Userid);
		$Mobile = $this->Db->Escape($_Mobile);
		$this->Db->Query("
			UPDATE `user`
			SET `mobile` = '{$Mobile}'
			WHERE `id` = '{$Userid}';
		");
	}
	public function SetName($_Userid, $_Name) {
		$Userid = $this->Db->Escape($_Userid);
		$Name = $this->Db->Escape($_Name);
		$this->Db->Query("
			UPDATE `user`
			SET `name` = '{$Name}'
			WHERE `id` = '{$Userid}';
		");
	}
	public function Get($_Userid) {
		$Userid = $this->Db->Escape($_Userid);
		$Result = $this->Db->Query("
			SELECT `id`, `username`, `name`, `mobile`, `time` FROM `user`
				WHERE
					`id` = '{$Userid}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object) : (false);
	}
}
