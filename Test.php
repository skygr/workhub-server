<?php
include('Frame.php');
include('Config.php');

/*
var_dump(Frame::getModel('Session')->GetUserid());
var_dump(Frame::getModel('Session')->SetUserid('100'));
var_dump(Frame::getModel('Session')->GetUserid());
var_dump(Frame::getModel('Session')->DelUserid());
var_dump(Frame::getModel('Session')->GetUserid());
*/

/*
Frame::runRouter('/Reg', array('Username' => '123456', 'Password' => '123456'));
*/

/*
Frame::runRouter('/Login', array('Username' => '123456', 'Password' => '123'));
Frame::runRouter('/Login', array('Username' => '123456', 'Password' => '123456'));
var_dump(Frame::getModel('Session')->GetUserid());
Frame::runRouter('/Logout', array());
var_dump(Frame::getModel('Session')->GetUserid());
*/

/*
Frame::runRouter('/Login', array('Username' => '123456', 'Password' => '123456'));
Frame::runRouter('/Set/Name', array('Name' => '王光睿'));
Frame::runRouter('/Set/Mobile', array('Mobile' => '1886810192600'));
Frame::runRouter('/Set/Password', array('Password' => 'skygr123'));
*/

/*
Frame::runRouter('/UserInfo/-1', array());
*/

/*
Frame::runRouter('/SelfInfo', array());
Frame::runRouter('/Login', array('Username' => '123456', 'Password' => 'skygr123'));
Frame::runRouter('/SelfInfo', array());
*/

/*
Frame::runRouter('/Login', array('Username' => '123456', 'Password' => 'skygr123'));
Frame::runRouter('/Group/Append', array('Name' => 'TIC-Club'));
*/

/*
Frame::runRouter('/Group/Search/10/0', array('Name' => '腾讯'));
*/

/*
$Name = array('王光睿', '陈昊哲', '杜鹃');
$Mobile = array('18868101926', '18868100350', '18868100308');
for($i = 0; $i < 3; $i++) {
	Frame::runRouter('/Act/Reg', array('Username' => "testuser-{$i}", 'Password' => "testuser-{$i}"));
	Frame::runRouter('/Act/Login', array('Username' => "testuser-{$i}", 'Password' => "testuser-{$i}"));
	Frame::runRouter('/Self/SetName', array('Name' => $Name[$i]));
	Frame::runRouter('/Self/SetMobile', array('Mobile' => $Mobile[$i]));
	Frame::runRouter('/Group/Apply/2', array());
}
*/

/*
Frame::runRouter('/Act/Login', array('Username' => "testuser-0", 'Password' => "testuser-0"));
Frame::runRouter('/Task/Append/2/3', array('Name' => '123456', 'Content' => '123456', 'EndTime' => '95.06.08'));
*/
Frame::runRouter('/Act/Login', array('Username' => 'testuser-0', 'Password' => 'testuser-0'));
Frame::runRouter('/Group/User/3', array());
