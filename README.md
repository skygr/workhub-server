# workhub-server

## 介绍

基于LAMP架构实现的团队任务管理工具服务器后端，采用Frame.php开源MVC框架。

## 服务器信息

* `Protocol`: `http`
* `IP`: `121.43.235.226`
* `Port`: `82`

## Restful-APIs

全部`API`基于`HTTP/1.1`协议，服务器后端将通过`Json`形式返回数据。
通用接口形式如下:

	{
		'desc': string(code),
		'data': ...
	}

所有接口将在合法调用时返回`('desc' = '0')`，`data`字段为符合接口逻辑的一段描述或数据集。
客户端需合理的控制连续接口请求的`Cookie`一致性，并正确响应服务端回传的`Cookie`。

### 注册

* `URL`: `/Act/Reg`
* `POST`:
	* `Username`: `账号`
	* `Password`: `密码`
* `JSON`:
	* 注册成功
	
			{
				'desc': '0',
				'data': 'Reg success.'
			}
			
	* 账号已经存在

			{
				'desc': '1',
				'data': 'Username is already exist.'
			}
			
	* 账号或密码长度过短
	
			{
				'desc': '2',
				'data': 'Username and password must be more than 5 characters.'
			}

### 登录

* `URL`: `/Act/Login`
* `POST`:
	* `Username`: `账号`
	* `Password`: `密码`
* `JSON`:
	* 登录成功

			{
				'desc': '0',
				'data': 'Login success.'
			}
			
	* 账号或密码错误
			
			{
				'desc': '1',
				'data': 'Username or password wrong.'
			}

### 退出

* `URL`: `/Act/Logout`
* `POST`: `empty`
* `JSON`:
	* 退出成功

			{
				'desc': '0',
				'data': 'Logout success.'
			}
			
	* 尚未登录

			{
				'desc': '1',
				'data': 'You are not login yet.'
			}

### 修改姓名

* `URL`: `/Self/SetName`
* `POST`:
	* `Name`: `姓名`
* `JSON`:
	* 修改成功

			{
				'desc': '0',
				'data': 'Set name success.'
			}
			
	* 尚未登录

			{
				'desc': '1',
				'data': 'You are not login yet.'
			}

### 修改手机号

* `URL`: `/Self/SetMobile`
* `POST`:
	* `Mobile`: `手机号`
* `JSON`:
	* 修改成功
			
			{
				'desc': '0',
				'data': 'Set mobile success.'
			}
			
	* 格式错误

			{
				'desc': '1',
				'data': 'Mobile format wrong.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data'" 'You are not login yet.'
			}
			
### 修改密码

* `URL`: `/Self/SetPassword`
* `POST`:
	* `Password`: `密码`
* `JSON`:
	* 修改成功
			
			{
				'desc': '0',
				'data': 'Set password success.'
			}
			
	* 密码长度过短
	
			{
				'desc': '1',
				'data': 'Password must be more than 5 characters.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 获取个人信息

* `URL`: `/Self/Info`
* `POST`: `empty`
* `JSON`:
	* 获取成功

			{
				'desc': '0',
				'data': {
					'id': string,
					'username': string,
					'name': string,
					'mobile': string,
					'time': string
				}
			}
			
		 * `id`: `用户编号`
		 * `username`: `账号`
		 * `name`: `姓名`
		 * `mobile`: `手机号`
		 * `time`: `注册时间`
		
	* 尚未登录

			{
				'desc': '1',
				'data': 'You are not login yet.'
			}

### 获取用户信息

* `URL`: `/User/Info/{Userid}`
	* `Userid`: 用户编号
* `POST`: `empty`
* `JSON`:
	* 获取成功

			{
				'desc': '0',
				'data': {
					'id': string,
					'username': string,
					'name': string,
					'mobile': string,
					'time': string
				}
			}
			
		 * `id`: `用户编号`
		 * `username`: `账号`
		 * `name`: `姓名`
		 * `mobile`: `手机号`
		 * `time`: `注册时间`
		 
	* 用户不存在

			{
				'desc': '1',
				'data': 'Useid is not exist.'
			}

### 创建团队

* `URL`: `/Group/Append`
* `POST`:
	* `Name`: `团队名`
* `JSON`:
	* 创建成功

			{
				'desc': '0',
				'data': int
			}
			
		* `data`: 团队编号
		
	* 团队名过短
			
			{
				'desc': '1',
				'data': 'Name must be more than 5 characters.'
			}

### 搜索团队

* `URL`: `/Group/Search/{Limit}/{Offset}`
	* `Limit`: `结果集的大小`
	* `Offset`: `结果集的偏移`
* `POST`:
	* `Name`: `搜索词`
* `JSON`:
	* 搜索成功
			
			{
				'desc': '0',
				'data': [
					{
						'id': string,
						'name': string,
						'time': string,
						'creator_id': string,
						'creator_username': string,
						'creator_name': string
					}
				]
			}
			
		* `id`: 团队编号
		* `name`: 团队名称
		* `time`: 团队创建时间
		* `creator_id`: 创建者id
		* `creator_username`: 创建者账号
		* `creator_name`: 创建者姓名

### 修改团队名

* `URL`: `/Group/SetName/{Groupid}`
	* `Groupid`: `团队编号`
* `POST`:
	* `Name`: `团队名`
* `JSON`:
	* 修改成功

			{
				'desc': '0',
				'data': 'Set name success of this group.'
			}
			
	* 团队名过短
			
			{
				'desc': '1',
				'data': 'Name must be more than 5 characters.'
			}
			
	* 团队不属于当前用户

			{
				'desc': '2',
				'data': ''You are the owener of this group.'
			}
	
	* 尚未登录

			{
				'desc': '3',
				'data': ''You are not login yet.'
			}

### 删除团队

* `URL`: `/Group/Remove/{Groupid}`
	* `Groupid`: `团队编号`
* `POST`: `empty`
* `JSON`:
	* 删除成功
		
			{
				'desc': '0',
				'data': 'Remove group success.'
			}
			
	* 团队不属于当前用户

			{
				'desc': '1',
				'data': 'You are not the owener of this group.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 申请加入团队

* `URL`: `/Group/Apply/{Groupid}`
	* `Groupid`: `团队编号`
* `POST`: `empty`
* `JSON`:
	* 申请成功

			{
				'desc': '0',
				'data': 'Apply success.'
			}
			
	* 当前用户是团队创建者
			
			{
				'desc': '1',
				'data': ''You are the owener of this group.'
			}
			
	* 当前用户已经是团队成员

			{
				'desc': '2',
				'data': 'You have already in this group.'
			}	
			
	* 申请已经提交，正在审核

			{
				'desc': '3',
				'data': 'Do not apply twice.'
			}
			
	* 尚未登录

			{
				'desc': '4',
				'data': 'You are not login yet.'
			}

### 同意加入申请

* `URL`: `/Group/Agree/{Relationid}`
	* `Relationid`: `团队成员关系编号`
* `POST`: `empty`
* `JSON`:
	* 同意成功

			{
				'desc': '0',
				'data': 'Agree success.'
			}
			
	* 当前用户不是团队创建者

			{
				'desc': '1',
				'data': 'You are not the owener of this group.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 拒绝加入申请

* `URL`: `/Group/Agree/{Relationid}`
	* `Relationid`: `团队成员关系编号`
* `POST`: `empty`
* `JSON`:
	* 拒绝成功

			{
				'desc': '0',
				'data': 'DisAgree success.'
			}
	
	* 当前用户不是团队创建者

			{
				'desc': '1',
				'data': 'You are not the owener of this group.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 获取当前用户所属的团队

* `URL`: `/Group/List`
* `POST`: `empty`
* `JSON`:
	* 获取成功
		
			{
				'desc': '0',
				'data': [
					{
						'id': string,
						'name': string,
						'time': string,
						'creator_id': string,
						'creator_username': string,
						'creator_name': string
					}
				]
			}
			
		* `id`: 团队编号
		* `name`: 团队名称
		* `time`: 团队创建时间
		* `creator_id`: 创建者id
		* `creator_username`: 创建者账号
		* `creator_name`: 创建者姓名
		
	* 尚未登录

			{
				'desc': '1',
				'data': 'You are not login yet.'
			}

### 离开团队

* `URL`: `/Group/Leave/{Groupid}`
	* `Groupid`: `团队编号`
* `POST`: `empty`
* `JSON`:
	* 离开成功

			{
				'desc': '0',
				'data': 'Leave success.'
			}
			
	* 当前用户不是团队成员

			{
				'desc': '1',
				'data': 'You are not the member of this group.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 列举团队成员

* `URL`: `/Group/User/{Groupid}`
	* `Groupid`: `团队成员`
* `POST`: `empty`
* `JSON`:
	* 获取成功

			{
				'desc': '0',
				'data': [
					{
						id: string,
						name: string,
						username: string
					}
				]
			}
			
		* `id`: 用户编号
		* `name`: 用户姓名
		* `username`: 用户账号
	
	* 当前用户不是团队成员

			{
				'desc': '1',
				'data': 'You are not the member of this group.'
			}
			
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 增加任务

* `URL`: `/Task/Append/{Groupid}/{Userids}`
	* `Groupid`: `团队编号`
	* `Userids`: 用`,`分隔的用户编号组
* `POST`:
	* `Title`: `任务标题`
	* `Content`: `任务描述`
* `JSON`:
	* 增加成功

			{
				'desc': '0',
				'data': int
			}
		
		* `data`: 任务编号
	
	* `Userids`中存在不属于这一个团队的用户

			{
				'desc': '1',
				'data': 'User with id {Userid} is not in this group.'
			}
	
	* 标题和正文的长度过短

			{
				'desc': '2',
				'data': 'Title and content must be more than 5 characters.'
			}
			
	* 任务接收者为空

			{
				'desc': '3',
				'data': 'This task has no receiver.'
			}
	
	* 当前用户不属于这个团队

			{
				'desc': '4',
				'data': 'You are not in this group.'
			}
			
	* 尚未登录

			{
				'desc': '5',
				'data': 'You are not login yet.'
			}

### 回复任务

* `URL`: `/Task/Reply/{Taskid}`
	* `Taskid`: `任务编号`
* `POST`:
	* `Content`: `回复内容`
* `JSON`:
	* 回复成功
		
			{
				'desc': '0',
				'data': 'Reply Success.'
			}
	
	* 当前用户不是这个任务的接受者或创建者

			{
				'desc': '1',
				'data': 'You are not the member of this task.'
			}
	
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 删除任务

* `URL`: `/Task/Remoe/{Taskid}`
	* `Taskid`: `任务编号`
* `POST`: `empty`
* `JSON`:
	* 删除成功

			{
				'desc': '0',
				'data': 'Remove success.'
			}
			
	* 当前用户不是任务创建者

			{
				'desc': '1',
				'data': 'You are not the creator of this group.'
			}
	
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}

### 获取与当前用户参与的全部任务

* `URL`: `/Task/List`
* `POST`: `empty`
* `JSON`:
	* 获取成功

			{
				'desc': '0',
				'data': [
					{
						'creator_id': string,
						'creator_username': string,
						'creator_name': string,
						'id': string,
						'time': string,
						'title': string,
						'content': string
					}
				]
			}
		* `creator_id`: `创建者编号`
		* `creator_username`: `创建者账号`
		* `creator_name`: `创建者姓名`
		* `id`: `任务编号`
		* `time`: `创建时间`
		* `title`: `任务标题`
		* `content`: `任务描述`
	
	* 尚未登录

			{
				'desc': '1',
				'data': 'You are not login yet.'
			}

### 获取指定任务下的全部回复

* `URL`: `/Task/ListReply/{Taskid}`
* `POST`: `empty`
* `JSON`:
	* 获取成功

			{
				'desc': '0',
				'data': [
					{
						'userid': string,
						'name': string,
						'username': string,
						'content': string,
						'time': string
					}
				]
			}
		* `userid`: 用户编号
		* `name`: 用户姓名
		* `username`: 用户账号
		* `content`: 回复内容
		* `time`: 回复时间
	
	* 当前用户不是指定任务的创建者或接受者

			{
				'desc': '1',
				'data': 'You can not view this task.'
			}
	
	* 尚未登录

			{
				'desc': '2',
				'data': 'You are not login yet.'
			}