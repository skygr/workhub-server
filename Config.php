<?php

//Path Definition
Frame::addModelPath(__DIR__.'/App/Mds');
Frame::addViewEnginePath(__DIR__.'/App/Ves');
Frame::addControllerPath(__DIR__.'/App/Cts');

//Api Routers
Frame::addRouterRule('Api', 'Reg',				'/Act/Reg');
Frame::addRouterRule('Api', 'Login',			'/Act/Login');
Frame::addRouterRule('Api', 'Logout',			'/Act/Logout');
Frame::addRouterRule('Api', 'SetName',			'/Self/SetName');
Frame::addRouterRule('Api', 'SetMobile',		'/Self/SetMobile');
Frame::addRouterRule('Api', 'SetPassword',		'/Self/SetPassword');
Frame::addRouterRule('Api', 'SelfInfo',			'/Self/Info');
Frame::addRouterRule('Api', 'UserInfo',			'/User/Info/{Userid}'); 
Frame::addRouterRule('Api', 'GroupAppend',		'/Group/Append');
Frame::addRouterRule('Api', 'GroupSearch',		'/Group/Search/{Limit}/{Offset}');
Frame::addRouterRule('Api', 'GroupSetName',		'/Group/SetName/{Groupid}');
Frame::addRouterRule('Api', 'GroupRemove',		'/Group/Remove/{Groupid}');
Frame::addRouterRule('Api', 'GroupApply',		'/Group/Apply/{Groupid}');
Frame::addRouterRule('Api', 'GroupGetApply',    '/Group/GetApply');
Frame::addRouterRule('Api', 'GroupAgree',		'/Group/Agree/{Relationid}');
Frame::addRouterRule('Api', 'GroupDisAgree',    '/Group/DisAgree/{Relationid}');
Frame::addRouterRule('Api', 'GroupList',		'/Group/List');
Frame::addRouterRule('Api', 'GroupLeave',		'/Group/Leave/{Groupid}');
Frame::addRouterRule('Api', 'GroupUser',		'/Group/User/{Groupid}');
Frame::addRouterRule('Api', 'TaskAppend',		'/Task/Append/{Groupid}/{Userids}');
Frame::addRouterRule('Api', 'TaskReply',		'/Task/Reply/{Taskid}');
Frame::addRouterRule('Api', 'TaskRemove',		'/Task/Remove/{Taskid}');
Frame::addRouterRule('Api', 'TaskList',			'/Task/List');
Frame::addRouterRule('Api', 'TaskListReply',	'/Task/ListReply/{Taskid}');
