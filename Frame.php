<?php

// error_reporting(0);

abstract class Frame_Model {
	final protected function getModel($_ModelName) {
		return Frame::getModel($_ModelName);
	}
	final protected function Warn($_Message) {
		Frame::Warn($_Message);
	}
	final protected function Error($_Message) {
		Frame::Error($_Message);
	}
}

abstract class Frame_ViewEngine {
	final protected function getView($_ViewName) {
		return Frame::getView($_ViewName);
	}
	final protected function getViewFilename($_ViewName) {
		return Frame::getViewFilename($_ViewName);
	}
	abstract public function View($_ViewName, $_ViewVariables);
}

abstract class Frame_Controller {
	public $Input = NULL;
	final protected function getModel($_ModelName) {
		return Frame::getModel($_ModelName);
	}
	final protected function getViewEngine($_ViewEngineName) {
		return Frame::getViewEngine($_ViewEngineName);
	}
}

final class Frame_Input {
	private $_PostVariables;
	private $_UrlVariables;
	public function __construct($_PostVariables = false, $_UrlVariables = false) {
		$this->_PostVariables = is_array($_PostVariables) ? ($_PostVariables) : array();
		$this->_UrlVariables = is_array($_UrlVariables) ? ($_UrlVariables) : array();
	}
	public function getPost($_VarName) {
		return isset($this->_PostVariables[$_VarName]) ? ($this->_PostVariables[$_VarName]) : (false);
	}
	public function getVar($_VarName) {
		return isset($this->_UrlVariables[$_VarName]) ? ($this->_UrlVariables[$_VarName]) : (false);
	}
}

final class Frame {
	
	/* Model */
	private static $ModelPaths = array();
	private static $ModelHandles = array();
	public static function getModel($_ModelName) {
		if(is_string($_ModelName)) {
			if(!isset(self::$ModelHandles[$_ModelName])) {
				for($_i = 0; $_i < count(self::$ModelPaths); $_i++) {
					if(file_exists(self::$ModelPaths[$_i].'/Frame_Model_'.$_ModelName.'.php')) {
						include(self::$ModelPaths[$_i].'/Frame_Model_'.$_ModelName.'.php');
						if(class_exists('Frame_Model_'.$_ModelName)) {
							$_ClassName = 'Frame_Model_'.$_ModelName;
							self::$ModelHandles[$_ModelName] = new $_ClassName;
							break;
						}
					}
				}
			}
			return isset(self::$ModelHandles[$_ModelName]) ? (self::$ModelHandles[$_ModelName]) : (false);
		} else {
			return false;
		}
	}
	public static function addModelPath($_ModelPath) {
		if(is_string($_ModelPath)) {
			array_push(self::$ModelPaths, $_ModelPath);
		}
	}

	/* View **/
	private static $ViewPaths = array();
	private static $ViewFilenames = array();
	private static $ViewContents = array();
	public static function getView($_ViewName) {
		if(is_string($_ViewName)) {
			if(!isset(self::$ViewContents[$_ViewName])) {
				for($_i = 0; $_i < count(self::$ViewPaths); $_i++) {
					if(file_exists(self::$ViewPaths[$_i].'/'.$_ViewName)) {
						self::$ViewContents[$_ViewName] = file_get_contents(self::$ViewPaths[$_i].'/'.$_ViewName);
						self::$ViewFilenames[$_ViewName] = self::$ViewPaths[$_i].'/'.$_ViewName;
					}
				}
			}
			return isset(self::$ViewContents[$_ViewName]) ? (self::$ViewContents[$_ViewName]) : (false);
		} else {
			return false;
		}
	}
	public static function getViewFilename($_ViewName) {
		if(is_string($_ViewName)) {
			if(!isset(self::$ViewFilenames[$_ViewName])) {
				for($_i = 0; $_i < count(self::$ViewPaths); $_i++) {
					if(file_exists(self::$ViewPaths[$_i].'/'.$_ViewName)) {
						self::$ViewFilenames[$_ViewName] = self::$ViewPaths[$_i].'/'.$_ViewName;
					}
				}
			}
			return isset(self::$ViewFilenames[$_ViewName]) ? (self::$ViewFilenames[$_ViewName]) : (false);
		} else {
			return false;
		}
	}
	public static function addViewPath($_ViewPath) {
		if(is_string($_ViewPath)) {
			array_push(self::$ViewPaths, $_ViewPath);
		}
	}

	/* ViewEngine */
	private static $ViewEnginePaths = array();
	private static $ViewEngineHandles = array();
	public static function getViewEngine($_ViewEngineName) {
		if(is_string($_ViewEngineName)) {
			if(!isset(self::$ViewEngineHandles[$_ViewEngineName])) {
				for($_i = 0; $_i < count(self::$ViewEnginePaths); $_i++) {
					if(file_exists(self::$ViewEnginePaths[$_i].'/Frame_ViewEngine_'.$_ViewEngineName.'.php')) {
						include(self::$ViewEnginePaths[$_i].'/Frame_ViewEngine_'.$_ViewEngineName.'.php');
						if(class_exists('Frame_ViewEngine_'.$_ViewEngineName)) {
							$_ClassName = 'Frame_ViewEngine_'.$_ViewEngineName;
							self::$ViewEngineHandles[$_ViewEngineName] = new $_ClassName;
							break;
						}
					}
				}
			}
			return isset(self::$ViewEngineHandles[$_ViewEngineName]) ? (self::$ViewEngineHandles[$_ViewEngineName]) : (false);
		} else {
			return false;
		}
	}
	public static function addViewEnginePath($_ViewEnginePath) {
		if(is_string($_ViewEnginePath)) {
			array_push(self::$ViewEnginePaths, $_ViewEnginePath);
		}
	}

	/* Controller */
	private static $ControllerPaths = array();
	private static $ControllerHandles = array();
	private static function getController($_ControllerName) {
		if(is_string($_ControllerName)) {
			if(!isset(self::$ControllerHandles[$_ControllerName])) {
				for($_i = 0; $_i < count(self::$ControllerPaths); $_i++) {
					if(file_exists(self::$ControllerPaths[$_i].'/Frame_Controller_'.$_ControllerName.'.php')) {
						include(self::$ControllerPaths[$_i].'/Frame_Controller_'.$_ControllerName.'.php');
						if(class_exists('Frame_Controller_'.$_ControllerName)) {
							$_ClassName = 'Frame_Controller_'.$_ControllerName;
							self::$ControllerHandles[$_ControllerName] = new $_ClassName;
							break;
						}
					}
				}
			}
			return isset(self::$ControllerHandles[$_ControllerName]) ? (self::$ControllerHandles[$_ControllerName]) : (false);
		} else {
			return false;
		}
	}
	public static function addControllerPath($_ControllerPath) {
		if(is_string($_ControllerPath)) {
			array_push(self::$ControllerPaths, $_ControllerPath);
		}
	}
	public static function runController($_ControllerName, $_MethodName, $_InputHandle) {
		if($_ControllerHandle = self::getController($_ControllerName)) {
			if(is_callable(array($_ControllerHandle, $_MethodName))) {
				$_ControllerHandle->Input = $_InputHandle;
				call_user_func(array($_ControllerHandle, $_MethodName));
			}	
		}
	}

	/* Error | Warn */
	public static function Warn($_Message) {
		echo $_Message."\n";
	}
	public static function Error($_Message) {
		echo $_Message."\n";
		die();
	}

	/* Router */
	private static $RouterRules = array();
	public static function addRouterRule($_ControllerName, $_MethodName, $_RouterRule) {
		if(is_string($_RouterRule) and is_string($_ControllerName) and is_string($_MethodName)) {
			array_push(self::$RouterRules, 
				array(
					'ControllerName' => $_ControllerName,
					'MethodName' => $_MethodName,
					'RouterRule' => $_RouterRule
				)
			);
		}
	}
	private static function cplRouter($_RouterRule, $_RequestUri) {
		if(is_string($_RouterRule) and is_string($_RequestUri)) {
	   		$_RuleParts = explode('/', $_RouterRule);
    		$_UriParts = explode('/', $_RequestUri);
			if(count($_RuleParts) != count($_UriParts)) {
    			return false;
    		} else {
    			$_Vars = array();
    			for($_i = 0; $_i < count($_RuleParts); $_i++) {
    				if((strlen($_RuleParts[$_i]) > 2) and ($_RuleParts[$_i][0] == '{')) {
    					$_Vars[substr($_RuleParts[$_i], 1, -1)] = urldecode($_UriParts[$_i]);
    				} else if($_RuleParts[$_i] != $_UriParts[$_i]) {
    					return false;
    				}
    			}
    			return $_Vars;
    		}
    	} else {
    		return false;
    	}
    }
    public static function getUrl($_ControllerName, $_MethodName, $_ControllerVars) {
    	if(is_string($_ControllerName) and is_array($_ControllerVars)) {
    		$_RequestUri = '/';
    		for($_i = 0; $_i < count(self::$RouterRules); $_i++) {
    			if((self::$RouterRules[$_i]['ControllerName'] == $_ControllerName) && (self::$RouterRules[$_i]['MethodName'] == $_MethodName)) {
    				$_RequestUri = self::$RouterRules[$_i]['RouterRule'];
    				break;
    			}
    		}
    		foreach($_ControllerVars as $_Key => $_Value) {
    			$_RequestUri = str_replace('{'.$_Key.'}', urlencode($_Value), $_RequestUri);
    		}
    		return $_RequestUri;
    	}
    }
    public static function runRouter($_RequestUri, $_PostVariables) {
    	for($_i = 0; $_i < count(self::$RouterRules); $_i++) {
    		if(($_UrlVariables = self::cplRouter(self::$RouterRules[$_i]['RouterRule'], $_RequestUri)) !== false) {
    			self::runController(self::$RouterRules[$_i]['ControllerName'], self::$RouterRules[$_i]['MethodName'], new Frame_Input($_PostVariables, $_UrlVariables));
			}
    	}
    } 
    public static function AutoRun() {
        self::runRouter($_SERVER['REQUEST_URI'], $_POST);
    }
 
}                                                                                                                  
