drop table if exists `user`;
create table if not exists `user` (
	`id` int primary key auto_increment,
	`username` varchar(32),
	`password` varchar(32),
	`name` varchar(32),
	`mobile` varchar(16),
	`time` timestamp default current_timestamp
);
drop table if exists `group`;
create table if not exists `group` (
	`id` int primary key auto_increment,
	`name` varchar(32),
	`time` timestamp default current_timestamp,
	`userid` int,
	foreign key (`userid`) references `user`(`id`) on delete cascade on update cascade
);
drop table if exists `group_relation`;
create table if not exists `group_relation` (
	`id` int primary key auto_increment,
	`groupid` int,
	`userid` int,
	`state` int,
	`time` timestamp default current_timestamp,
	foreign key (`groupid`) references `group`(`id`) on delete cascade on update cascade,
	foreign key (`userid`) references `user`(`id`) on delete cascade on update cascade
);
drop table if exists `task`;
create table if not exists `task` (
	`id` int primary key auto_increment,
	`title` varchar(32),
	`content` varchar(4096),
	`state` int, 
	`userid` int,
	`groupid` int,
	`time` timestamp default current_timestamp,
	foreign key (`userid`) references `user`(`id`) on delete cascade on update cascade,
	foreign key (`groupid`) references `group`(`id`) on delete cascade on update cascade
);
drop table if exists `task_relation`;
create table if not exists `task_relation` (
	`id` int primary key auto_increment,
	`taskid` int,
	`userid` int,
	`time` timestamp default current_timestamp,
	foreign key (`taskid`) references `task`(`id`) on delete cascade on update cascade,
	foreign key (`userid`) references `user`(`id`) on delete cascade on update cascade
);
drop table if exists `task_reply`;
create table if not exists `task_reply` (
	`id` int primary key auto_increment,
	`content` varchar(4096),
	`taskid` int,
	`userid` int,
	`time` timestamp default current_timestamp,
	foreign key (`taskid`) references `task`(`id`) on delete cascade on update cascade,
	foreign key (`userid`) references `user`(`id`) on delete cascade on update cascade
);


